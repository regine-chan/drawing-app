Drawing app made for assignment nr. 2.

This is a console application wrapped in a while loop. To exit, walk through the prompts and enter a input. When prompted if you want to continue or exit enter 0 to exit or 1 to continue.

Open Visual Studio 2019 and hit run program, the console window will pop up and instruct you on how to use the application.