﻿using System;
using System.Drawing;

namespace DrawingApp
{
    class Program
    {
        // Function that decides if filled or unfilled shape is drawn
        static void DrawShape(int length, int height, string drawingChar, bool fill)
        {
                
            if (fill)
            {
                DrawFilledShape(length, height, drawingChar);
            } else
            {
                DrawUnfilledShape(length, height, drawingChar);
            }
        }

        // Function that draws a filled shape
        static void DrawFilledShape(int length, int height, string drawingChar)
        {
            // Outputs each line to the console based on the height input
            for(int i = 0; i < height; i++)
            {
                // Initializes the string to be drawn
                string module = "";
                // Adds characets based on the length input
                for(int j = 0; j < length; j++)
                {
                    module += drawingChar;
                }
                // Prints the finished line
                Console.WriteLine(module);
            }
        }

        // Function that draws an unfilled shape
        static void DrawUnfilledShape(int length, int height, string drawingChar)
        {
            // Outputs each line to the console based on the height input
            for (int i = 0; i < height; i++)
            {
                // Initializes the line
                string module = drawingChar;
                // Checks if this is the first or last line to be drawn, if so fill with characters
                if (i == 0 || i == height - 1)
                {
                    for (int j = 0; j < length - 1; j++)
                    {
                        module += drawingChar;
                    }
                }
                // If not start or end line, then dont fill with characters
                else
                {
                    for (int j = 0; j < length - 2; j++)
                    {
                        module += " ";
                        if (j == length - 3)
                        {
                            module += drawingChar;
                        }
                    }
                }
                // print the line
                Console.WriteLine(module);
            }
        }

        static void Main(string[] args)
        {
            // Sets game state to active
            int activeState = 1;
            // Loops through the drawing app until user inputs 0 to stop.
            while (activeState == 1)
            {
                try
                {
                    // Welcome message
                    Console.WriteLine("This program lets you draw shapes from your own configuration");
                    // User can decide which character to draw with
                    Console.WriteLine("Please type a character to use in the drawing: ");
                    // Reads the character the user wants to use
                    string drawingChar = Console.ReadLine();
                    // User decides the length of the rectangle
                    Console.WriteLine("Please enter rectangle length: ");
                    // Reads the length
                    int length = int.Parse(Console.ReadLine());
                    // User decides the height of the rectangle
                    Console.WriteLine("Please enter rectangle height: ");
                    // reads the height
                    int height = int.Parse(Console.ReadLine());
                    // User can decide if drawing is to be filled
                    // Made both before the lecturer prompted us to make the unfilled shape
                    // Thus left both in
                    Console.WriteLine("Do you want to fill object? type true/false");
                    // reads if the user wants the shape filled
                    bool fill = bool.Parse(Console.ReadLine());

                    // Calls draw shape after all the parameters are set
                    // Draws both rectangle and square depending on the length and height input
                    DrawShape(length, height, drawingChar, fill);

                    // Prompts the user if they want to continuo drawing
                    Console.WriteLine("Type 1 to continue drawing or 0 to stop.");
                    // reads the if the user wants to continue drawing
                    activeState = int.Parse(Console.ReadLine());
                }
                catch (Exception e)
                {
                    // Prints any exception
                    Console.WriteLine(e.Message);
                }
            }
            
        }

        
    }
}


